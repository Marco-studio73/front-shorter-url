import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShorterUrlComponent } from './components/shorter-url/shorter-url.component';


const routes: Routes = [
  {
    path:'shorter-url', component: ShorterUrlComponent
  },
  {
    path: '', redirectTo: '/shorter-url', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
