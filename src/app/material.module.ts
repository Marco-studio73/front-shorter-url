import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        FlexLayoutModule,
        MatInputModule
    ],
    exports: [
        FlexLayoutModule,
        MatInputModule
    ]
})
export class MaterialModule {}