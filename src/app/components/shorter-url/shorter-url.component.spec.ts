import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShorterUrlComponent } from './shorter-url.component';

describe('ShorterUrlComponent', () => {
  let component: ShorterUrlComponent;
  let fixture: ComponentFixture<ShorterUrlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShorterUrlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShorterUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
