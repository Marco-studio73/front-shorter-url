// modules
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// route
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// component
import { ShorterUrlComponent } from './components/shorter-url/shorter-url.component';

@NgModule({
  declarations: [
    AppComponent,
    ShorterUrlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
