import { TestBed } from '@angular/core/testing';

import { ShorterUrlService } from './shorter-url.service';

describe('ShorterUrlService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShorterUrlService = TestBed.get(ShorterUrlService);
    expect(service).toBeTruthy();
  });
});
